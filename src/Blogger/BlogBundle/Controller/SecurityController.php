<?php

namespace Blogger\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Security;

class SecurityController extends Controller
{
    public function loginAction()
    {
        if ($this->has(Security::AUTHENTICATION_ERROR)) {
            $error = $this->get(Security::AUTHENTICATION_ERROR);
        } else {
            $error = $this->get('session')->get(Security::AUTHENTICATION_ERROR);
        }

        return $this->render('BloggerBlogBundle:Security:login.html.twig', array(
            'email' => $this->get('session')->get(Security::LAST_USERNAME),
            'error' => $error
        ));
    }
}