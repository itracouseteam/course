<?php
// src/Blogger/BlogBundle/Controller/PageController.php

namespace Blogger\BlogBundle\Controller;

use Blogger\BlogBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Blogger\BlogBundle\Entity\Enquiry;
use Blogger\BlogBundle\Form\EnquiryType;


class PageController extends Controller
{
    public function aboutAction()
    {
        return $this->render('BloggerBlogBundle:Page:about.html.twig');
    }
    public function blogAction()
    {
        $em = $this->getDoctrine()
            ->getManager();

        $blogs = $em->getRepository('BloggerBlogBundle:Blog')
            ->getLatestBlogs();

        return $this->render('BloggerBlogBundle:Page:blog.html.twig', array(
            'blogs' => $blogs
        ));
    }
    public function postAction()
    {
        return $this->render('BloggerBlogBundle:Page:post.html.twig');
    }

    public function contactAction(Request $request, $id)
    {
        $enquiry = new User();

        $form = $this->createForm(EnquiryType::class, $enquiry);

        if ($request->isMethod($request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $user = $em->getRepository('BloggerBlogBundle:User')->find($id);

                if (!$user) {
                    throw $this->createNotFoundException('Eror with UserConrtoller.php');
                }


                return $this->$user;
                return $this->redirect($this->generateUrl('BloggerBlogBundle_homepage'));
            }
        }

        return $this->render('BloggerBlogBundle:Page:contact.html.twig', array(
            'form1' => $form->createView(), 'form2' => $form->createView()
        ));
    }


}