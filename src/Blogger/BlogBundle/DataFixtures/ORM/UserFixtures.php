<?php
// src/Blogger/BlogBundle/DataFixtures/ORM/UserFixtures.php

namespace Blogger\BlogBundle\DataFixtures\ORM;

use Blogger\BlogBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Blogger\BlogBundle\Entity\Category;
use Blogger\BlogBundle\Entity\Post;
use Blogger\BlogBundle\Entity\Tag;
use Blogger\BlogBundle\Entity\Role;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class UserFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        // создание роли ROLE_ADMIN
        $role = new Role();
        $role->setName('ROLE_ADMIN');

        $manager->persist($role);

        // создание пользователя
        $user = new User();
        $user->setEmail('admin@admin.admin');
        $user->setPassword('admin');
        $user->setSalt(md5(time()));

        // шифрует и устанавливает пароль для пользователя,
        // эти настройки совпадают с конфигурационными файлами
        $encoder = new MessageDigestPasswordEncoder('sha512', true, 10);
        $password = $encoder->encodePassword('admin', $user->getSalt());
        $user->setPassword($password);

        $user->getUserRoles()->add($role);

        $manager->persist($user);

        $manager->flush();



//        $user1 = new User();
//        $user1->setEmail('test@test.test');
//        $user1->setPassword('test');
//        $user1->setRule('3');
//        $manager->persist($user1);
//        $manager->flush();
//
//        $user2 = new User();
//        $user2->setEmail('test2@test2.test2');
//        $user2->setPassword('test2');
//        $user2->setRule('2');
//        $manager->persist($user2);
//        $manager->flush();
//
//        $user3 = new User();
//        $user3->setEmail('test1@test1.test1');
//        $user3->setPassword('test1');
//        $user3->setRule('1');
//        $manager->persist($user3);
//        $manager->flush();
//
//        $user4 = new User();
//        $user4->setEmail('test0@test0.test0');
//        $user4->setPassword('test0');
//        $user4->setRule('0');
//        $manager->persist($user4);
//        $manager->flush();
//
//        $this->addReference('user-1', $user1);
//        $this->addReference('user-2', $user2);
//        $this->addReference('user-3', $user3);
//        $this->addReference('user-4', $user4);
    }

    public function getOrder()
    {
        return 1;
    }
}